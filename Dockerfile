FROM node:lts-alpine
WORKDIR /usr/src/app
RUN npm install --only=production
COPY . .
ENTRYPOINT [ "node", "index.js" ]
