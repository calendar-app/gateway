const express = require('express')
const app = express()
const port = process.env.GATEWAY_PORT;
const httpProxy = require('express-http-proxy')

function getProxy(host, subpath) {
  return httpProxy(host, { proxyReqPathResolver: subpath ? req => subpath + req.url : null });
}

app.use('/_healthz', (_, res) => res.status(200).send());
app.use('/api', getProxy('http://api:7000'));
app.use('/', getProxy('http://frontend'));

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
